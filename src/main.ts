import {Action, Game} from "./models/Game";
import {debug} from "./services/debug";

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 *
 */

const game = new Game();

let tour = 0;

// game loop
while (true) {

    game.setTour(tour++);

    // @ts-ignore
    const actionCount = parseInt(readline(), 10); // the number of spells and recipes in play
    for (let i = 0; i < actionCount; i++) {
        // @ts-ignore
        const inputs = readline().split(' ');
        const actionId = parseInt(inputs[0], 10); // the unique ID of this spell or recipe
        const actionType = inputs[1]; // in the first league: BREW; later: CAST, OPPONENT_CAST, LEARN, BREW
        const delta0 = parseInt(inputs[2], 10); // tier-0 ingredient change
        const delta1 = parseInt(inputs[3], 10); // tier-1 ingredient change
        const delta2 = parseInt(inputs[4], 10); // tier-2 ingredient change
        const delta3 = parseInt(inputs[5], 10); // tier-3 ingredient change
        const price = parseInt(inputs[6], 10); // the price in rupees if this is a potion
        const tomeIndex = parseInt(inputs[7], 10); // in the first two leagues: always 0; later: the index in the tome if this is a tome spell, equal to the read-ahead tax; For brews, this is the value of the current urgency bonus
        const taxCount = parseInt(inputs[8], 10); // in the first two leagues: always 0; later: the amount of taxed tier-0 ingredients you gain from learning this spell; For brews, this is how many times you can still gain an urgency bonus
        const castable = inputs[9] !== '0'; // in the first league: always 0; later: 1 if this is a castable player spell
        const repeatable = inputs[10] !== '0'; // for the first two leagues: always 0; later: 1 if this is a repeatable player spell

        game.addAction({
            actionId,
            actionType,
            delta0,
            delta1,
            delta2,
            delta3,
            price,
            tomeIndex,
            taxCount,
            castable,
            repeatable,
            alreadyUse: false
        } as Action);

    }
    for (let i = 0; i < 2; i++) {
        // @ts-ignore
        const inputs = readline().split(' ');
        const inv0 = parseInt(inputs[0], 10); // tier-0 ingredients in inventory
        const inv1 = parseInt(inputs[1], 10);
        const inv2 = parseInt(inputs[2], 10);
        const inv3 = parseInt(inputs[3], 10);
        const score = parseInt(inputs[4], 10); // amount of rupees

        if(i===0) {
            game.setIngredients({inv0, inv1, inv2, inv3});
        }
    }

    // in the first league: BREW <id> | WAIT; later: BREW <id> | CAST <id> [<times>] | LEARN <id> | REST | WAIT
    console.log(game.nextTurn());
}
