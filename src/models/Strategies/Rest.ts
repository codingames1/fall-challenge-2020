import {Strategy} from "./Strategy";

export class Rest extends Strategy {

    public nextTurn(): string {
        return `REST`;
    }

    public canPlayStrategy(): boolean {
        return true;
    }
}
