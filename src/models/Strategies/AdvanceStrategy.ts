import {Strategy} from "./Strategy";
import {Action} from "../Game";

export abstract class AdvanceStrategy extends Strategy {

    protected abstract getActionsToPlay(): Action[]

    public canPlayStrategy(): boolean {
        return this.getActionsToPlay().length > 0;
    }

    protected actionToArray(actions: any): Array<Action> {
        const val = [];
        Object.keys(actions).forEach(key => {
            val.push(actions[key]);
        });

        return val;
    }

    protected enoughIngredients(item: Action): boolean {
        return item.delta0 + this.ingredients.inv0 >= 0 &&
            item.delta1 + this.ingredients.inv1 >= 0 &&
            item.delta2 + this.ingredients.inv2 >= 0 &&
            item.delta3 + this.ingredients.inv3 >= 0;
    }

    protected enoughSpaceInInventory(item: Action): boolean {
        return (this.sumIngredients() + AdvanceStrategy.sumDelta(item)) < 10;
    }

    protected sumIngredients(): number {
        return this.ingredients.inv0 + this.ingredients.inv1 + this.ingredients.inv2 + this.ingredients.inv3;
    }

    protected static sumDelta(item: Action): number {
        return item.delta0 + item.delta1 + item.delta2 + item.delta3;
    }

}
