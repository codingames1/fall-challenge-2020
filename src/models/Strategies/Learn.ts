import {Action} from "../Game";
import {AdvanceStrategy} from "./AdvanceStrategy";
import {debug} from "../../services/debug";

export class Learn extends AdvanceStrategy {

    public nextTurn(): string {
        const actions = this.getActionsToPlay();
        const actionToPlay = actions[actions.length-1];
        this.learnActions[actionToPlay.actionId].alreadyUse = true;

        return `LEARN ${actionToPlay.actionId}`;
    }

    public canPlayStrategy(): boolean {
        return this.getActionsToPlay().length > 0;
    }

    protected getActionsToPlay(): Action[] {
        return this
            .actionToArray(this.learnActions)
            .filter(item => {
                return item.alreadyUse !== true;
            })
            .filter(item => {
                return this.enoughIngredients(item);
            });
    }

}
