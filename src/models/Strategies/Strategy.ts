import {Ingredients} from "../Game";

export abstract class Strategy {

    protected ingredients: Ingredients;
    protected readonly castActions: any = {};
    protected readonly brewActions: any = {};
    protected readonly opponentCastActions: any = {};
    protected readonly learnActions: any = {};

    constructor({ingredients, castActions, brewActions, opponentCastActions, learnActions}) {
        this.ingredients = ingredients;
        this.castActions = castActions;
        this.brewActions = brewActions;
        this.opponentCastActions = opponentCastActions;
        this.learnActions = learnActions;
    }

    public abstract nextTurn(): string

    public abstract canPlayStrategy(): boolean

}
