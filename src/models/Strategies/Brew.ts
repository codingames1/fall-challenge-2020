import {Action} from "../Game";
import {AdvanceStrategy} from "./AdvanceStrategy";

export class Brew extends AdvanceStrategy {

    public nextTurn(): string {
        const actionToPlay = this.getActionsToPlay()[0];
        this.brewActions[actionToPlay.actionId].alreadyUse = true;

        return `BREW ${actionToPlay.actionId}`;
    }

    protected getActionsToPlay(): Action[] {
        return this
            .sortActions()
            .filter(item => {
                return item.alreadyUse !== true;
            })
            .filter(item => {
                return Math.abs(item.delta0) <= this.ingredients.inv0 &&
                    Math.abs(item.delta1) <= this.ingredients.inv1 &&
                    Math.abs(item.delta2) <= this.ingredients.inv2 &&
                    Math.abs(item.delta3) <= this.ingredients.inv3;
            });
    }

    private sortActions(): Array<Action> {
        return this.actionToArray(this.brewActions).sort((a, b) => {
            return b.price - a.price;
        });
    }

}
