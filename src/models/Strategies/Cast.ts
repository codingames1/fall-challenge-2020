import {Action} from "../Game";
import {AdvanceStrategy} from "./AdvanceStrategy";

export class Cast extends AdvanceStrategy {

    public nextTurn(): string {
        const actions = this.getActionsToPlay();
        const actionToPlay = actions[actions.length-1];

        return `CAST ${actionToPlay.actionId}`;
    }

    protected getActionsToPlay(): Action[] {
        return this
            .actionToArray(this.castActions)
            .filter(item => {
                return item.castable;
            })
            .filter(item => {
                return this.enoughIngredients(item);
            })
            .filter(item => {
                return this.enoughSpaceInInventory(item);
            });
    }

}
