import {Brew} from "./Strategies/Brew";
import {Strategy} from "./Strategies/Strategy";
import {Cast} from "./Strategies/Cast";
import {Rest} from "./Strategies/Rest";
import {Learn} from "./Strategies/Learn";

export class Game {

    private ingredients: Ingredients;
    protected readonly castActions: any = {};
    protected readonly brewActions: any = {};
    protected readonly opponentCastActions: any = {};
    protected readonly learnActions: any = {};
    public tour: number = 0;

    public nextTurn(): string {
        let strategy: Strategy;
        const params = {
            ingredients : this.ingredients,
            castActions: this.castActions,
            brewActions: this.brewActions,
            opponentCastActions : this.opponentCastActions,
            learnActions: this.learnActions
        };
        const learn = new Learn(params);
        const brew = new Brew(params);
        const cast = new Cast(params);

        if(this.tour < 5) {
            return learn.nextTurn();
        }

        if(brew.canPlayStrategy()) {
            strategy = brew;
        } else if(cast.canPlayStrategy()) {
            strategy = cast;
        } else {
            strategy = new Rest(params);
        }

        return strategy.nextTurn();
    }

    public addAction(action: Action): void {
        switch(action.actionType) {
        case "BREW":
            this.brewActions[action.actionId] = action;
            break;
        case "CAST":
            this.castActions[action.actionId] = action;
            break;
        case "LEARN":
            this.learnActions[action.actionId] = action;
            break;
        default:
            this.opponentCastActions[action.actionId] = action;
        }
    }

    public setIngredients({inv0, inv1, inv2, inv3}: Ingredients): void {
        this.ingredients = {inv0, inv1, inv2, inv3};
    }

    public setTour(tour): void {
        this.tour = tour;
    }
}

export type Action = {
    actionId: number
    actionType: string
    delta0: number
    delta1: number
    delta2: number
    delta3: number
    price: number
    tomeIndex: number
    taxCount: number
    castable: boolean
    repeatable: boolean
    alreadyUse: boolean
}

export type Ingredients = { inv2: number; inv1: number; inv3: number; inv0: number }
